extends Node2D

export(String) var flag = "test"
var taken=false

var stage_controller = null

func _ready():
	var finder = preload("res://managers/finder/Finder.gd").new(get_tree())

	if finder.has_stage_controller():
		stage_controller = finder.get_stage_controller()


func _on_Ultra_key_collected(body):
	if not taken:
		taken = true
		$Sprite.hide()
		$Particles2D.emitting = true
		$Timer.start()

		if stage_controller != null:
			stage_controller.add_flag(flag)


func _on_Timer_timeout():
	get_parent().queue_free()
