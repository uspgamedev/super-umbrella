extends Node2D

var taken=false

func _on_key_collected(body):
	if not taken:
		taken = true
		body.get_node("Collector").keys += 1
		$KeyCounter.set_text(str(body.get_node("Collector").keys))
		$Sprite.queue_free()
		$Timer.start()


func _on_Timer_timeout():
	get_parent().queue_free()
