extends Node

# Esse nó serve para "matar" a entidade no qual ele pertence
# Diferente do Damageable, que lida com o dano e similares,
# este nó lida com animações de morte e queue_free de entidades.

# Poderiamos dizer, então, que o destroyable lida com a vida,
# e o killable lida com a morte. Chefes com diversos estagios, 
# por exemplo, talvez tenham outro nó para lidar com as diversas
# transições. 

var entity

func _entity_ready(entity):
	self.entity = entity

# Função usada para "matar" o ser. Mostra uma animação com o
# o nome "dying" caso exista (no death_player) e destroi
# a entidade no fim da duração do death_timer

func _died():
	if $death_player.has_animation("dying"):
		$death_player.play("dying")
	$death_timer.start()

func _on_death_timer_timeout():
	entity.queue_free()
