extends "res://entities/components/platformer/PhysicsBase.gd"

const CONTROL = preload("res://controls/ControlBase.gd")
const PLATFORMER = preload("res://entities/components/platformer/Platformer.gd")
const STATES = preload("res://entities/components/states/States.gd")

func _requests_control(entity):
	return !entity.is_on_floor() and entity.get_component(PLATFORMER).velocity.y > 0 \
								 and entity.get_component(CONTROL).action_umbrella \
								 and not entity.get_component(STATES).isAttacking
