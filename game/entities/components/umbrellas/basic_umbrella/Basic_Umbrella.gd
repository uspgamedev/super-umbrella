extends Sprite

const UMB_STR = 6

var entity
var states
var control
var platformer

func _entity_ready(entity):
	self.entity = entity
	self.platformer = entity.get_component(
		preload("res://entities/components/platformer/Platformer.gd")
	)
	assert(self.platformer != null)
	self.states = entity.get_component(preload("res://entities/components/states/States.gd"))
	assert(self.states != null)
	self.control = entity.get_component(preload("res://controls/ControlBase.gd"))
	assert(self.control != null)

func _process(delta):
	if self.entity == null: return

	if $umbrella_animations.get_current_animation() != "attacking_left" \
	   and $umbrella_animations.get_current_animation() != "attacking_right":
		self.states.isAttacking = false

	if not self.states.isAttacking:
		if self.states.umbrellaActive:
			if $umbrella_animations.get_current_animation() != "falling":
				$umbrella_animations.play("falling")
		else:
			self.frame = 0

	if self.states.lookingRight:
		self.entity.get_node("Sprite").z_index   = 0
		self.z_index = 1
	else:
		self.entity.get_node("Sprite").z_index   = 1
		self.z_index = 0

func _physics_process(delta):
	if self.entity == null: return

	# Updates umbrellaActive state
	if not self.states.isAttacking and not self.states.onFloor and self.control.action_umbrella:
		if (self.platformer.velocity.y > 0): #is going down, y is positive
			self.states.umbrellaActive = true
		else:
			self.states.umbrellaActive = false
	else:
		self.states.umbrellaActive = false

func _input(event):
	if event.is_action_pressed("action_attack"):

		if self.states.lookingRight:
			$umbrella_animations.play("attacking_right")
		else:
			$umbrella_animations.play("attacking_left")

		self.states.isAttacking = true
		$RainDrops.restart()
