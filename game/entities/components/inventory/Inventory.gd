extends Node

#Enquanto não temos um save pronto
export(bool) var has_basic_umbrella = false
export(bool) var has_heavy_umbrella = false

#Resources
const BASIC_UMBRELLA_SOURCE = preload("res://entities/components/umbrellas/basic_umbrella/Basic_Umbrella.tscn")
const HEAVY_UMBRELLA_SOURCE = preload("res://entities/components/umbrellas/heavy_umbrella/Heavy_Umbrella.tscn")

#Player Info
var entity
const CONTROL = preload("res://controls/ControlBase.gd")
const PLATFORMER = preload("res://entities/components/platformer/Platformer.gd")
const STATES = preload("res://entities/components/states/States.gd")

#Inventory
var Inventory = []
var Inventory_Index = 0 #Guarda a posição da arma atual no inventario
var Old_Item #Guarda a posição da arma atual na cena

func _entity_ready(entity):
	self.entity = entity
	assert(self.entity != null)

	#Mudar para uma função de load depois aqui
	if has_basic_umbrella:
		Inventory.append(BASIC_UMBRELLA_SOURCE)

	if has_heavy_umbrella:
		Inventory.append(HEAVY_UMBRELLA_SOURCE)

	#Se existir algum item no inventario, adiciona ele ao jogador
	if (Inventory.size() > 0):
		var item = Inventory[0].instance()
		Old_Item = item
		entity.add_child(item)

		if item.has_method("_entity_ready"):
			item._entity_ready(entity) 


func _input(event):

	#Verifica se tem algum item no inventario para ser trocado
	if (Inventory.size() > 1):

		if (event.is_action_pressed("action_next_weapon")):
			change_item(Inventory_Index+1)

		if (event.is_action_pressed("action_previous_weapon")):
			change_item(Inventory_Index-1)


#Função que muda o item do jogador para o Inventory_Index dado
func change_item(Index):

	#Por enquanto, o jogador não pode trocar o seu item caso esteja
	#usando sua ação (isso gera um erro no physics quando ele tenta
	#pegar os atributos do item que não existem mais).

	if (entity.get_component(STATES).isAttacking or \
	  (!entity.is_on_floor() and \
		entity.get_component(PLATFORMER).velocity.y > 0 and
		entity.get_component(CONTROL).action_umbrella )):
			print("can't switch items right now!")
			return

	#Arruma o Index caso ele tenha saido do inventario
	if (Index >= Inventory.size()):
		Index = 0
	elif (Index < 0):
		Index = Inventory.size() - 1

	Inventory_Index = Index

	#Destroi o item antigo
	Old_Item.queue_free()

	#Adiciona o item novo
	var item = Inventory[Inventory_Index].instance()
	Old_Item = item
	entity.add_child(item)

	if item.has_method("_entity_ready"):
		item._entity_ready(entity) 