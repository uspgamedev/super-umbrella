extends Area2D

const DESTROYABLE = preload("res://entities/components/damageable/Damageable.gd")

export(int) var power = 1

func _hit(entity):
	var destroyable = entity.get_component(DESTROYABLE)
	if destroyable != null:
		destroyable.take_damage(power, entity.global_position - self.global_position)