extends Control

var stage_select = false #wheter or not a stage was already selected

signal stage_selected(name,path)

func _ready():
	get_tree().paused = false
	$FadeTransition.fade_in()
	yield($FadeTransition, "fade_completed")
	if $Stages.get_child_count():
		# Focuses first stage button
		for icon in $Stages.get_children():
			icon.connect("focused", self, "_on_StageIcon_focused")
			icon.connect("pressed", self, "_on_StageIcon_pressed")
		$Stages.get_child(0).get_node("Button").grab_focus()


func _on_StageIcon_focused(icon):
	# Hides an arrow if it's the first and/or last stage in the list
	if icon == $Stages.get_child(0):
		icon.hide_arrow(icon.ARROW_LEFT)
	if icon == $Stages.get_child($Stages.get_child_count() - 1):
		icon.hide_arrow(icon.ARROW_RIGHT)
	
	var x_0 = OS.get_real_window_size().x / 2 - icon.rect_size.x / 2
	$Tween.interpolate_property($Stages, "rect_position", $Stages.rect_position,
		Vector2(x_0 - icon.rect_position.x, $Stages.rect_position.y), .3,
		Tween.TRANS_EXPO, Tween.EASE_OUT)
	$Tween.start()


func _on_StageIcon_pressed(icon):
	if not stage_select:
		stage_select = true
		$FadeTransition.fade_out()
		yield($FadeTransition, "fade_completed")
		emit_signal("stage_selected", icon.stage_name, icon.stage_scene)
